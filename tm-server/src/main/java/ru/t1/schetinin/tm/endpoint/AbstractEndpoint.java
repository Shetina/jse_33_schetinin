package ru.t1.schetinin.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.api.service.IServiceLocator;
import ru.t1.schetinin.tm.api.service.IUserService;
import ru.t1.schetinin.tm.dto.request.AbstractUserRequest;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.exception.user.AccessDeniedException;
import ru.t1.schetinin.tm.model.Session;
import ru.t1.schetinin.tm.model.User;



public abstract class AbstractEndpoint {

    @NotNull
    protected Session check(@Nullable final AbstractUserRequest request, @Nullable final Role role){
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @NotNull final Session session = serviceLocator.getAuthService().validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw  new AccessDeniedException();
        return session;
    }

    @NotNull
    protected Session check(@Nullable final AbstractUserRequest request){
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return serviceLocator.getAuthService().validateToken(token);
    }

    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final  IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}

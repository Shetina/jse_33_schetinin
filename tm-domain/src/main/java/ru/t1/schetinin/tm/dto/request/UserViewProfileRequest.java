package ru.t1.schetinin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public class UserViewProfileRequest extends AbstractUserRequest {

    public UserViewProfileRequest(@Nullable final String token) {
        super(token);
    }

}
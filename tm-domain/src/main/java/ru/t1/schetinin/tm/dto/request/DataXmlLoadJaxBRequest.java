package ru.t1.schetinin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public class DataXmlLoadJaxBRequest extends AbstractUserRequest {

    public DataXmlLoadJaxBRequest(@Nullable final String token) {
        super(token);
    }

}
package ru.t1.schetinin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public class UserLogoutRequest extends AbstractUserRequest {

    public UserLogoutRequest(@Nullable final String token) {
        super(token);
    }

}

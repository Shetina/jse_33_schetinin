package ru.t1.schetinin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public class DataYamlSaveFasterXmlRequest extends AbstractUserRequest {

    public DataYamlSaveFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}
package ru.t1.schetinin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public class TaskClearRequest extends AbstractUserRequest {

    public TaskClearRequest(@Nullable final String token) {
        super(token);
    }

}
